package com.example.a10149.mytraining;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a10149.mytraining.com.javabean.OkHttp;

import java.io.IOException;

public class SignupActivity extends AppCompatActivity {
    private EditText mEditTextId;
    private EditText mEditTextPassword;
    private EditText mEditTextPasswordAgain;
    private Button mButtonSignup;
    private String mId_Signup;
    private String mPassword_Signup;
    private String mPasswordAgain_Signup;
    private String mPostReturn;
    private static final String TAG = "LandActivity";
    private void showWrong(String str){
        String show = str;
        Toast.makeText(this,show,Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mEditTextPassword = (EditText)findViewById(R.id.password_signup);
        mEditTextPasswordAgain = (EditText)findViewById(R.id.passwordAgain_signup);
        mButtonSignup = (Button)findViewById(R.id.button_signup);
        mEditTextId = (EditText)findViewById(R.id.id_signup);
        mId_Signup = mEditTextId.getText().toString();
        mPassword_Signup = mEditTextPassword.getText().toString();
        mPasswordAgain_Signup = mEditTextPasswordAgain.getText().toString();
        mButtonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String bank = "{\"id\":\""+mEditTextId.getText().toString()+"\",\"password\":\""+mEditTextPassword.getText().toString()+"\"}";
                final String url = "http://60.205.202.9:5000/signup";
                if(!mEditTextPassword.getText().toString().equals(mEditTextPasswordAgain.getText().toString())){
                    showWrong("两次输入的密码不同");
                }else{
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            OkHttp okHttp = new OkHttp();
                            try{
                                Log.i(TAG, "run: "+bank);
                                mPostReturn = okHttp.post(url,bank);
                                Log.i(TAG, "run: "+mPostReturn);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if(mPostReturn.equals("1")){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showWrong("注册成功");
                                    }
                                });
                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showWrong("注册出现不可预期问题");
                                    }
                                });
                            }
                        }
                    }).start();
                }

            }
        });
    }
}
