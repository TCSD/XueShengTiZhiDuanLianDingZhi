package com.example.a10149.mytraining;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a10149.mytraining.com.javabean.OkHttp;
import com.example.a10149.mytraining.com.javabean.MakeJson;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LandActivity extends AppCompatActivity {
    private Button mButtonLogin;
    private Button mButtonSignup;
    private EditText mEditTextId;
    private EditText mEditTextPassword;
    private static final String TAG = "LandActivity";
    private String s = "";


    void showWrong2(){
        int show = R.string.wrong2;
        Toast.makeText(this,show,Toast.LENGTH_SHORT).show();
    }
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land);

        mButtonLogin = (Button)findViewById(R.id.login);
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditTextId = (EditText)findViewById(R.id.id);
                mEditTextPassword = (EditText)findViewById(R.id.password);
                //使用Gson实现转换为Json数据类型
                Map<String,String> map = new HashMap<String,String>();
                map.put("id",mEditTextId.getText().toString());
                map.put("password",mEditTextPassword.getText().toString());
                MakeJson mMakeJson = new MakeJson();
                final String a = mMakeJson.makeJson(map);


//                final String ss = "{\"id\":\""+mEditTextId.getText().toString()+"\",\"password\":\""+mEditTextPassword.getText().toString()+"\"}";
                final String url = "http://60.205.202.9:5000";

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        OkHttp okHttp = new OkHttp();
                        try {
                            Log.i(TAG, "run: "+a);
                            s = okHttp.post(url,a);
                            Log.i(TAG, "run: "+s);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if(s.equals("1")){
                            Intent i = new Intent();
                            i.setClass(LandActivity.this,MainActivity.class);
                            startActivity(i);
                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showWrong2();
                                }
                            });
                        }
                    }
                }).start();




            }
        });
        mButtonSignup = (Button)findViewById(R.id.signup);
        mButtonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(LandActivity.this,SignupActivity.class);
                startActivity(i);
            }
        });



    }
}
