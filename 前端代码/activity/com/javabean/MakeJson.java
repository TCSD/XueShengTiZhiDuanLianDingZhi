package com.example.a10149.mytraining.com.javabean;

import com.google.gson.Gson;

import java.util.Map;

/**
 * Created by 10149 on 2017/11/9.
 */

public class MakeJson {
    public Map<String,String> mMap;

    public Map<String, String> getMap() {
        return mMap;
    }

    public void setMap(Map<String, String> map) {
        mMap = map;
    }

    public String makeJson(Map<String,String> map){
        mMap = map;
        Gson gson = new Gson();
        return gson.toJson(mMap);
    }

}
