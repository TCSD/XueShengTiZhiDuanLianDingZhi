package com.example.a10149.mytraining.com.javabean;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by 10149 on 2017/11/2.
 */

public class OkHttp {
    OkHttpClient client = new OkHttpClient();
    public MediaType JSON = MediaType.parse("application/json; charset=utf-8");


    public String post(String url,String json)throws IOException{
        RequestBody body = RequestBody.create(JSON,json);
        Request request = new Request.Builder().url(url).post(body).build();
        Response response = client.newCall(request).execute();
        if(response.isSuccessful()){
            return response.body().string();
        }else {
            throw new IOException("Unexpected code"+response);
        }
    }
}
